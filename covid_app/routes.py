"""Route declaration."""
from flask import current_app as app
from flask import render_template


@app.route('/')
def home():
    return render_template('/pages/index.html')
                                
@app.route('/volunteer')
def volunteer():
    return render_template('forms/volunteer.html')
    
@app.route('/login')
def login():
    return render_template('forms/login.html')
    
@app.route('/register')
def register():
    return render_template('forms/register.html')
    
@app.route('/about')
def about():
    return render_template('pages/about.html')
    
@app.route('/ngo_profile')
def ngo_profile():
    return render_template('admin/ngo/ngo_details.html')
    
@app.route('/ngo_tasks')
def ngo_tasks():
    return render_template('admin/ngo/ngo_tasks.html')
    
@app.route('/vol_profile')
def vol_profile():
    return render_template('admin/volunteer/volunteer_details.html')
    
@app.route('/vol_tasks')
def vol_tasks():
    return render_template('admin/volunteer/vol_tasks.html')
    
@app.route('/help')
def help():
    return render_template('pages/help.html')

@app.route('/apply')
def apply():
    return render_template('forms/apply.html')
    
@app.route('/listings')
def listings():
    return render_template('pages/help_listing.html')
    
@app.route('/tasks')
def tasks():
    return render_template('pages/tasks.html')
    
@app.route('/error')
def error():
    return render_template('pages/404.html')
    
@app.route('/helpline')
def helpline():
    return render_template('pages/helpline.html')
    
@app.route('/ind-register')
def ind_register():
    return render_template('forms/ind_register.html')
    
@app.route('/grp-register')
def grp_register():
    return render_template('forms/grp_register.html')
    
@app.route('/couldnt')
def couldnt():
    return render_template('forms/couldnt.html')
    
@app.route('/join-us')
def join():
    return render_template('forms/join.html')
    
@app.route('/need-vol')
def need_vol():
    return render_template('forms/need_vol.html')
    
@app.route('/ngo-register')
def ngo_register():
    return render_template('forms/ngo.html')